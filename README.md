# Senior design 

Welcome to our 2019-2020 Cedarville Universtiy Senior Design repository! This repo contains several branches related to the development of the neural network and other software programs, along with branches for the GUI development.

#### Listing and Description of Branches ####
- **GUI:** Full GUI development branch. The path to the python GUI is ~/JavaGUI/src/seniordesign/pysimplegui.py. Within ~/JavaGUI/src/seniordesign/ you will also find takePicture.py, videoFeed.py, and animationPlayer.py. animationPlayer.py was meant to be a helper program to integrate the hand animations into the GUI. Contained also are several different model files that we saved from the neural network, the hand animations that we were going to integrate into the network, and a folder of data that contains all the images taken while using/testing the kiosk. The original Java GUI is also in the ~/JavaGUI directory.
- **RPSRecognition:** Uses our custom data generator to augment images. Saves the model file to disk. 
- **crop\_by\_depth:** This contains a script (crop\_by\_depth.py) to filter pixles out of an image based on depth. It uses the color image and the corresponding depth image to darken pixels that are too far away from the camera. Currently the script is not set up to save the filtered images anywhere.
- **noTimer:** Partial GUI development branch. This branch contains a bare-bones GUI used exclusively for data collection. There is not countdown tiemr on this branch, so image capture is instant.
- **transferLearning:** This branch contains code for transfer learning. It also has all of the crop\_by\_depth files along with some results from the filtering script. The transfer learning code is in the 'covnNetTest.py' file and is commented out. This file also contains the code to our final model that we used to train our network and is uncommented.

#### Enviornment Setup ####
In order to run the files on the non-GUI branches, you need to set up a python enviornment. 
- Install python 3 on your linux machine using:

        sudo apt upgrade
        sudo apt update
        sudo apt-get install python3.6

- Follow this guide in order to setup Anaconda on your linux machine: https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart
- Once you have configured Anaconda, create and activate a new enviornment using:

        conda create -n myenv python=3.6
        conda activate myenv

- Now that your Anaconda enviornment is activated, install keras and tensorflow. If you have an NVIDIA GPU, run:

        conda install -c anaconda keras-gpu

- If you only have a CPU, run:

        conda install -c anaconda keras

- This should install all the keras and tensorflow libraries needed. Next, you will have to install all the dependent libraries used in the project:

        numpy, os, imutils, cv2, sklearn, and any other libraries it asks for. The installs for these can be googled; you can generally try 'conda install yourlib' or 'sudo apt-get install yourlib'. If these don't work, google knows how. 

- Now you should be able to run the files through your console or using VSCode. If you are using VSCode, you need to select your conda enviornment as the interpreter. 

For the GUI branches:
- You will need to install the pyrealsense repository following this guide in order to operate the camera:

        https://github.com/IntelRealSense/librealsense/tree/master/wrappers/python

- Make sure you install this in the directory containing your file that needs this library. 

You can now run the files on both the GUI and non-GUI branches. 



#### Notes from FA_2019 ####
Current plans:\
Use a CNN to do image recognition\
Don't worry about Kiosk yet, a dataset is more important\
On that note, a quick Google search for "rock paper scissors image dataset" gives a lot of good results. Here are some that I found after a quick search:
- https://www.kaggle.com/drgfreeman/rockpaperscissors **Unused**
- https://www.kaggle.com/sanikamal/rock-paper-scissors-dataset **Unused**
- https://www.kaggle.com/alishmanandhar/rock-scissor-paper/version/1 **This is the one we went with. Used in master branch**
- https://github.com/alessandro-giusti/rock-paper-scissors **Unused**

Someone already did our project!
- https://www.reddit.com/r/MachineLearning/comments/b8htjt/p_rock_paper_scissors_with_artificial_intelligence/
- https://towardsdatascience.com/tutorial-using-deep-learning-and-cnns-to-make-a-hand-gesture-recognition-model-371770b63a51
    
Things to consider: no set background for our image capturing so that our image recognition has more applications outside of just our kiosk. Also consider video processing of the hand so that the timing is easier to handle. **This was our initial plan.**

Use ImageNet for initial CNN **We used a much simpler model than this.**

Helpful:

- https://towardsdatascience.com/transfer-learning-with-convolutional-neural-networks-in-pytorch-dd09190245ce


AWS (remote network training):
- https://aws.amazon.com/ec2/pricing/on-demand/ - pricing
- https://aws.amazon.com/ec2/instance-types/ - description 

g4dn.8xlarge seems promising

Depth camera for gesture recognition 

Guide for image manipulation in keras:
- https://machinelearningmastery.com/how-to-load-convert-and-save-images-with-the-keras-api/

Tensorboard command:
- tensorboard --logdir==training:logs --host=127.0.0.1
 
Data augmentation:
- https://machinelearningmastery.com/how-to-configure-image-data-augmentation-when-training-deep-learning-neural-networks/
    