import keras
import tensorflow
import tensorflow as tf
from keras import layers
from keras import models
from keras import optimizers
from keras.datasets import mnist
from keras import callbacks
from keras.utils import to_categorical
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import array_to_img
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
import datetime
import os
import pathlib
from imutils import paths
import cv2
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
import augment

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.80)
config = tf.ConfigProto(gpu_options=gpu_options)
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

########################################################################################
# Step 1: Load in images from datbase and preprocess 

rockDir = '/Users/jakekenniv/Desktop/senior-design/final/train/c0/'
paperDir = '/Users/jakekenniv/Desktop/senior-design/final/train/c1/'
scissorDir = '/Users/jakekenniv/Desktop/senior-design/final/train/c2/'

data_gen = augment.Generator.createGenerator(rockDir, 
											 paperDir, 
											 scissorDir, 
											 Batch_size=2, 
											 zoom=0.2, 
											 doHorizontalFlips=True, 
											 doVerticalFlips=True, 
											 augmentBrightness=0, 
											 addBlur=0.2, 
											 addNoise=0.1, 
											 doRotation=360)


########################################################################################
# Step 2: Load images into model adn train 

# define model 
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(500, 500, 3)))

model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))

model.add(layers.Flatten())
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(3, activation='sigmoid'))

rmsprop = optimizers.adam(lr = 0.00005)
reduce_lr = callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.8,
                              patience=5, min_lr=0.000001)

model.compile(optimizer=rmsprop,loss='categorical_crossentropy',metrics=['accuracy'])

# tensorboard
logdir = "logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
file_writer = tensorflow.summary.FileWriter(logdir)

# fit model
history = model.fit_generator(
	data_gen,
	steps_per_epoch=100,
	callbacks = [tensorboard_callback],
	epochs=30)

model.summary()